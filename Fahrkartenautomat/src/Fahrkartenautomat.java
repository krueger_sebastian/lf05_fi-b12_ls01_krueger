﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       double anzahlDerTickets;
       
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       System.out.print("Anzahl der Tickets ( 1 bis 5): ");
       anzahlDerTickets = tastatur.nextDouble();
       System.out.print("Zu zahlender Betrag (EURO): " + (zuZahlenderBetrag * anzahlDerTickets) + "\n");

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.00;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag * anzahlDerTickets)
       {
    	   System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag * anzahlDerTickets - eingezahlterGesamtbetrag) + "0");
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\n" + (anzahlDerTickets) + " Fahrscheine werden ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag * anzahlDerTickets;
       if(rückgabebetrag > 0.00)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + "0" + " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt: ");

           while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2;
           }
           while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1;
           }
           while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.50;
           }
           while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.20;
           }
           while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.10;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
           while(rückgabebetrag >= 3)// 3 Tickets
           {
        	   System.out.println("3"); 
        	   rückgabebetrag -= 3.0;
           }
           while(rückgabebetrag >= 4)//4 Tickets
           {
        	   System.out.println("4");
        	   rückgabebetrag -= 4.0;
           }
           while(rückgabebetrag >= 5)//5 Tickets
           {
        	   System.out.println("5");
        	   rückgabebetrag -= 5.0;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}

/*
5. Dadurch, dass sowie die Münzen, als auch die Tickets eine Zahl sind,
sollten die Datentypen der beiden Werte identisch sein.

6. Bei der Berechnung "anzahl * einzelpreis" wird im großen und ganzen
die Anzahl der Tickets mit dem Einzelpreis der Tickets Multipliziert.
Es entsteht also ein neuer Wert der durch die Berechnung entstanden ist,
womit man nun weitere Berechnungen durchführen kann.
*/